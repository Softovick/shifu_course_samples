var express = require('express');
var router = express.Router();
var list = [
  "test1",
  "test2",
  "test3",
  "test4",
  "test5"
];

/* GET list array page. */
router.get('/', function (req, res, next) {
  console.log(list);
  res.render('array', {
    title: 'List array',
    list: list
  });
});

router.get('/:id', function (req, res, next) {
  console.log(req.params.id);
  res.render('item', {
    number: req.params.id,
    value: list[Number(req.params.id)]
  });
});

module.exports = router;