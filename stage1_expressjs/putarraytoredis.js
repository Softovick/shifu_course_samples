var redis = require('redis'),
    client = redis.createClient(32768);

client.on("error", function (err) {
    console.log("Error " + err);
});

client.select(1, function (err) {
    if (err) console.log(err);
    client.del('zlist1');
    client.zadd('zlist1', 0, 'test1');
    client.zadd('zlist1', 1, 'test2');
    client.zadd('zlist1', 2, 'test3');
    client.zadd('zlist1', 3, 'test4');
    client.zadd('zlist1', 4, 'test5');
    client.zadd('zlist1', 5, 'test6');
    client.zcard('zlist1', function (err, reply){
        console.log(reply);
    });
    client.zrange('zlist1',0,-1,function (err, reply){})
    // client.hset('hashset1', 'field1', Date.now());
    // client.hset('hashset1', 'field2', 'test field 2 for hashset1');
    // client.hset('hashset2', 'field1', 'test field 1 for hashset2');
    client.quit();
});