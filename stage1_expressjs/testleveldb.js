var level = require('level');
var db = level('./mydb');

db.put('name', 'LevelDB', function (err) {
    if (err) return console.log('Ooops!', err);
    db.get('name', function (err, value) {
        if (err) return console.log('Ooops!', err);
        console.log('name=' + value);
    });
})