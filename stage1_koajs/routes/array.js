const router = require('koa-router')()

var list = [
  "test1",
  "test2",
  "test3",
  "test4",
  "test5"
];
router.prefix('/array')

router.get('/', async(ctx, next) => {
  await ctx.render('array', {
    title: 'LIST!',
    list: list
  })
})

router.get('/:id', async(ctx, next) => {
  console.log('request - ' + ctx.params.id);
  await ctx.render('item', {
    number: Number(ctx.params.id),
    value: list[Number(ctx.params.id)]
  })
})


module.exports = router