exports.install = function () {
	F.route('/', view_index);
	F.route('/list', view_list);
	F.route('/list/{id}', view_item);
	// or
	// F.route('/');
};

var list = [
	"test1",
	"test2",
	"test3",
	"test4",
	"test5"
];

function view_index() {
	var self = this;
	self.view('index');
}

function view_list() {
	var self = this;
	self.view('list', list);
}

function view_item(id) {
	var self = this;
	var model = {
		id: id,
		value: list[Number(id)]
	};

	self.view('item', model);
}